<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use Carbon\CarbonPeriod;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface
{
    public function calculate($bookingDateTime, $responseDateTime, $officeHours)
    {
        $startDate = new \DateTime($bookingDateTime);
        $endDate = new \DateTime($responseDateTime);

        $period = CarbonPeriod::create($startDate->format('Y-m-d'), $endDate->format('Y-m-d'));

        $totalTime = 0;

        foreach ($period as $date) {
            $dayOfTheWeek = $date->dayOfWeek;

            if ($officeHours[$dayOfTheWeek]['isClosed']) {
                continue;
            }

            $workStart = strtotime($officeHours[$dayOfTheWeek]['from']);
            $workEnds = strtotime($officeHours[$dayOfTheWeek]['to']);

            if ($startDate->format('Y-m-d') === $endDate->format('Y-m-d')) {

                $startHour = strtotime($startDate->format('H:i'));
                $endHour   = strtotime($endDate->format('H:i'));

                if ($startHour > $workEnds) {
                    // 0
                } elseif ($endHour < $workStart) {
                    // 0
                } elseif($startHour < $workStart && $endHour > $workStart && $endHour < $workEnds) {
                    $totalTime = round(($endHour - $workStart) / 60, 2);
                } elseif ($startHour > $workStart && $startHour < $workEnds && $endHour > $workEnds) {
                    $totalTime = round(($workEnds - $startHour) / 60, 2);
                } elseif (($startHour >= $workStart) && ($endHour <= $workEnds)) {
                    $totalTime = round(($endHour - $startHour) / 60, 2);
                } elseif ($startHour < $workStart && $endHour > $workEnds) {
                    $totalTime = round(($workEnds - $workStart) / 60, 2);
                }

            } else {

                $startHour = strtotime($startDate->format('00:00'));
                $endHour   = strtotime($endDate->format('23:59'));

                if ($startHour > $workEnds) {
                    // 0
                } elseif ($endHour < $workStart) {
                    // 0
                } elseif($startHour < $workStart && $endHour > $workStart && $endHour < $workEnds) {
                    $totalTime = round(($endHour - $workStart) / 60, 2);
                } elseif ($startHour > $workStart && $startHour < $workEnds && $endHour > $workEnds) {
                    $totalTime = round(($workEnds - $startHour) / 60, 2);
                } elseif (($startHour >= $workStart) && ($endHour <= $workEnds)) {
                    $totalTime = round(($endHour - $startHour) / 60, 2);
                } elseif ($startHour < $workStart && $endHour > $workEnds) {
                    $totalTime = round(($workEnds - $workStart) / 60, 2);
                }

            }
        }

        return $totalTime;
    }
}
