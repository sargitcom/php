<?php

namespace Tests\Unit;

use Proexe\BookingApp\Utilities\ResponseTimeCalculator;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingResponseTimeTest extends TestCase
{
    /** @test */
    public function testTask4()
    {
        $rtp = new ResponseTimeCalculator();

        $workinghours = [
            0 => [
                'isClosed' => false,
                'from' => '10:00',
                'to' => '11:00'
            ]
        ];

        $time = $rtp->calculate("2010-10-10 13:00", "2010-10-10 15:00", $workinghours);

        $workinghours = [
            0 => [
                'isClosed' => false,
                'from' => '16:00',
                'to' => '18:00'
            ]
        ];

        $time = $rtp->calculate("2010-10-10 13:00", "2010-10-10 15:00", $workinghours);

        $this->assertEquals(0, $time);

        $workinghours = [
            0 => [
                'isClosed' => false,
                'from' => '13:00',
                'to' => '15:00'
            ]
        ];

        $time = $rtp->calculate("2010-10-10 13:00", "2010-10-10 15:00", $workinghours);

        $this->assertEquals(120, $time);

        $workinghours = [
            0 => [
                'isClosed' => false,
                'from' => '15:01',
                'to' => '15:30'
            ]
        ];

        $time = $rtp->calculate("2010-10-10 13:00", "2010-10-10 15:00", $workinghours);

        $this->assertEquals(0, $time);


        $workinghours = [
            0 => [
                'isClosed' => false,
                'from' => '15:00',
                'to' => '15:30'
            ],
            1 => [
                'isClosed' => false,
                'from' => '10:00',
                'to' => '12:30'
            ],
            2 => [
                'isClosed' => false,
                'from' => '11:00',
                'to' => '16:30'
            ]

        ];

        $time = $rtp->calculate("2010-10-10 13:00", "2010-10-12 15:00", $workinghours);

        $this->assertEquals(330, $time);
    }
}
