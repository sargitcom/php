<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Illuminate\Console\Command;
use Proexe\BookingApp\Bookings\Models\BookingModel;
use Proexe\BookingApp\Utilities\ResponseTimeCalculator;

class CalculateResponseTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bookingApp:calculateResponseTime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates response time';

    /**
     * @var ResponseTimeCalculator
     */
    protected $responseTimeCalculator;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ResponseTimeCalculator $responseTimeCalculator)
    {
        parent::__construct();

        $this->responseTimeCalculator = $responseTimeCalculator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $bookings = BookingModel::with('office')->get()->toArray();

        if (empty($bookings)) {
            return;
        }

        foreach ($bookings as $booking) {
            $totalTime = $this->responseTimeCalculator->calculate($booking['created_at'], $booking['updated_at'], $booking['office']['office_hours']);

            $this->line("Total time for office {$booking['office']['name']}: {$totalTime}");
        }

	    //Use ResponseTimeCalculator class for all calculations
	    //You can use $this->line() to write out any info to console
    }
}
