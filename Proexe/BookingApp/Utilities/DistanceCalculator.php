<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 *
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

class DistanceCalculator
{
    /**
     * @param array  $from
     * @param array  $to
     * @param string $unit - m, km
     *
     * @return mixed
     */
    public function calculate($from, $to, $unit = 'm')
    {
        $distanceInMeters = $this->getHaversineGreatCircleDistanceInMeters($from[0], $from[1], $to[0], $to[1]);

        if ($unit === 'km') {
            return $distanceInMeters / 1000;
        }

        return $distanceInMeters;
    }

    /**
     * @param array $from
     * @param array $offices
     *
     * @return array
     */
    public function findClosestOffice($from, $offices)
    {
        if (empty($offices) || !is_array($from) || !array_key_exists(0, $from) || !array_key_exists(1, $from)) {
            return [];
        }

        $closestOffice = null;

        foreach ($offices as $office) {
            if ($closestOffice === null
                || $this->getHaversineGreatCircleDistanceInMeters($from[0], $from[1], $office['lat'], $office['lng'])
                   < $this->getHaversineGreatCircleDistanceInMeters($from[0], $from[1], $closestOffice['lat'], $closestOffice['lng'])) {
                $closestOffice = $office;
            }
        }

        return $closestOffice['name'];
    }

    protected function getHaversineGreatCircleDistanceInMeters(
        $latitudeFrom,
        $longitudeFrom,
        $latitudeTo,
        $longitudeTo,
        $earthRadius = 6371000
    ) {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $angle * $earthRadius;
    }

    /*
     * To calculate distance between two point in mySQL (at least versio 5.7) we can use such code
     *
     * select ST_Distance_Sphere(point(-14.22222218, 41.9631174),point(-41.9631174, 41.9631174)
     *
     * that is the simplest and fastest sollution. We could also convert php code to sql code and user query/stored procedure for calulations.
     */
}
